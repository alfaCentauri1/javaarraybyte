package com.alfaCentauri;

import java.lang.reflect.Field;

public class Ejecutable {
    public static void main(String []args){
        Class tipo = Document.class;
        Field[] fields = tipo.getDeclaredFields();
        for (Field field : fields) {
            Class<?> cls = field.getType();
            if (cls.isAssignableFrom(byte[].class)) {
                System.out.println(field.getName() + ": It's a byte array");
            }
            else {
                System.out.println(field.getName() + ": No is a byte array");
            }
        }
    }
}
